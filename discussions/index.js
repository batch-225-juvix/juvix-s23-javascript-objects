console.log('hello world');

/*let grades = [91.1, 92.2, 93,3];
console.log(grades);*/

// 89.2 means value

// OBJECTS
// naka,curly braces
// (`${key}: ${value}`);
/*--------------------------------------*/

// I.
// [SECTION] OBJECTS
/*
	- An object is a data type that is used to represent real world objects.
	- Information that we are stored in object are represented in a key "key:value" pair.
	- Another characteristics, A 'key' is also mostly reffered to as a "property of an object".
	- Different data types may be stored in an object's property creating complex data structures.


	SYNTAX:
		let objectName = {
			keyA: valueA,
			keyB: valueB

		}
*/
// Q: how about const at var po di sila pwede sa object?
// Objects represents of what value.
// 'key' is palatandaan.

// for example:
let cellphone = {
	name: 'Nokia 3210',
	manufactureDate: 1999,
	price: 100
};
console.log('Result from creating objects');
console.log(cellphone);
console.log(typeof cellphone);


// typeof is chenicheck niya of what kind of variable.

// Q: medyo complex po yung pag-ta-traverse through object sir noh, unlike sa array?
// Ans: YES

// pwede rin tayo mag dagdag ng price


// like sa array [], hindi detailed.
// let cellphone = ['Nokie 3210', 1999, 100];

/*------------------------------------*/


// II.
// Creating objects using a constructor function.
/*
	- Creating a reusable function to create several objects that have the same data structure.
	- this is useful for creating multiple instances/copies of an object.
	- An instance is a concrete occurence of any object  which emphasizes on the distinct/unique identity.


	SYNTAX:
		function ObjectName(keyA, keyB){
			this.keyA = keyA;
			this.keyB = keyB;
		}

*/
// The 'this' keyword allows to assign a new object's properties by associating them with values recieved from a constructor function's parameters.

// Laptop na kulay skyblue is our constructor.

// for example:
function Laptop(name, manufactureDate, price){
	this.name = name;
	this.manufactureDate = manufactureDate;
	this.price = price;



	// return {name: name, manufactureDate: manufactureDate, price: price}
}

// How to invoke in function.
// Laptop('name', 'manufactureDate', 'price');


// the 'new' operator creates an instance of an object.

let laptop = new Laptop('Lenovo', 2008, 20000);
console.log('Result from creating objects using object constructors:')
console.log(laptop);

// the Big letter L Laptop means function. We need to call the Laptop.
// magkaiba ang laptop at Laptop.
// ('Lenovo', 2008) means Parameter.
/*// function Laptop(name, manufactureDate){
	this.name = name;
	this.manufactureDate = manufactureDate;
}

 means that these are our Blueprint.
*/

//new means dapat merong new para hindi mag undefined.
// mas assell kapag nag lagay tayo ng return kung ayaw mo mag lagay ng 'new'
// return {name: name, manufactureDate: manufactureDate, price: price}
// yung 'this.' po ba pwedeng gamit outside function? ans: YES kc parang global yang "this". parang hoisting.


// if we add more let mylaptop. We can also put it. So it means it is reusable.
let myLaptop = new Laptop('MacBook Air', 2020, 30000);
console.log('Result from creating objects using object constructors:');
console.log(myLaptop);


/*--------------------------------*/

// [SECTION] Accessing Object Properties

// Using the dot notation - this is for the Best practice.
// The dot notation in here is .name

console.log('Result from dot notation: ' + myLaptop.name);
// we call or invoke the 'MacBook Air'


// Using the square notation
console.log('Result from square bracket notation: ' + myLaptop['name'])
// We do not recommended this. - Bad practice.

/*-------------------------*/
/*// Accessing array objects

	- Accessing object properties using square bracket notation and indexes can cause confusion.
	- By using the dot notation, this easily helps us differentiate accessing elements from arrays and properties from objects.


	SYNTAX:
		// if we access array
		array/Variable[index].name



		*/


// for example:


// let laptop = new Laptop('Lenovo', 2008, 20000);
// let myLaptop = new Laptop('MacBook Air', 2020, 30000);



let array  = [laptop, myLaptop];

console.log(array[0]['price']);
console.log(array[0].name);

// sa loob ng array [] is tinatawag na nating object such as laptop, myLaptop. //
// sa loob ng [] laptop is constructor.
// [0] means index.
// ['price'] means properties and not array kapag nasa loob ng console.log.
// .name sa loob ng console.log means yung dot notation natin. Ang purpose dyan is kapag tatawagin natin yung name.

// another example:
let sample = ['Hello', {firstName: 'CJ', lastName: 'Macaraeg'}, 78];

console.log(sample[1]);

console.log(sample[1].lastName);


/*--EXPLANATION--*/
// sa  loob ng array pwede kayo mag create ng object
// 78 means array
// {} means gagamit tayo ng dot notation.
// 'Hello' means [0] or index 0
// [1] means index
// index 1 or [1] na index means yung firstName at lastName
// yung index 2 or [2] means yung 78 
// kasi nahahati yung dalawa [{}]
// dapat naka .lastName kapag nag invoke sa loob ng console.log or tinatawag nating dot notation.
// lalagyan natin ng [1] index 1 sa loob ng () console.log kapag gusto natin mag invoke ng pangalan or lastName.


/*---------------------------------*/


// V.
// Initializing/adding object properties using dot notation


car.name = 'Honda Civic';
console.log('Result from adding properties using dot notation:');
console.log(car);

// Initializing/adding object properties using bracket notation
/*
    - While using the square bracket will allow access to spaces when assigning property names to make it easier to read, this also makes it so that object properties can only be accesssed using the square bracket notation
- This also makes names of object properties to not follow commonly used naming conventions for them
*/

/*----------------------*/



// [Section] Initializing/Adding/Deleting/Reassigning Object Properties
/*
    - Like any other variable in JavaScript, objects may have their properties initialized/added after the object was created/declared
    - This is useful for times when an object's properties are undetermined at the time of creating them
*/

let car = {};
// Initializing/adding object properties using bracket notation
car.name = 'Honda Civic'
console.log('Result from adding properties using dot notation:');
console.log(car);

/*
    - While using the square bracket will allow access to spaces when assigning property names to make it easier to read, this also makes it so that object properties can only be accesssed using the square bracket notation
- This also makes names of object properties to not follow commonly used naming conventions for them
*/

// Adding object properties
// for example:
// car.manufacture_date = 2019;
// console.log(car);

car['manufacture date'] = 2019;
console.log('Result from adding properties using bracket notation:')
console.log(car);

// Deleting object properties
delete car['manufacture date'];
console.log('Result deleting properties':);
console.log(car);


// Reassigning object properties
car.name = 'Dodge Charger R/T';
console.log('Result from reassigning properties:');
console.log(car); 

/*---------------------------*/

// [Section] Object Methods
/*
    - A method is a function which is a property of an object
    - They are also functions and one of the key differences they have is that methods are functions related to a specific object
    - Methods are useful for creating object specific functions which are used to perform tasks on them
    - Similar to functions/features of real world objects, methods are defined based on what an object is capable of doing and how it should work
*/

/* Specific function ito

talk: function (){
        console.log('Hello my name is ' + this.name);

*/

 /*

 Note: If you access an object method without (), it will return the function definition:

for example:
 function() { return this.firstName + " " + this.lastName; }

 Note:
 tapos ito yung tinatawag na statement:
  console.log('Hello my name is ' + this.name);

Note: 'John' is the key value.
Note: Ang + this.name is pang kuha lang ng 'John'
 */

let person = {
    name: 'John',
    talk: function (){
        console.log('Hello my name is ' + this.name);
    }
}

console.log(person);
console.log('Result from object methods:');
person.talk();

// Adding methods to objects
person.walk = function() { 
    console.log(this.name + ' walked 25 steps forward.');
};
person.walk();

// Methods are useful for creating reusable functions that perform tasks related to objects
let friend = {
    firstName: 'Joe',
    lastName: 'Smith',
    address: {
        city: 'Austin',
        country: 'Texas'
    },
    emails: ['joe@mail.com', 'joesmith@email.xyz'],
    introduce: function() {
        console.log('Hello my name is ' + this.firstName + ' ' + this.lastName);
    }
};

friend.introduce();

console.log(friend.address.city);
console.log(friend.emails[0])

// pwede rin kayo mag create ng array emails: ['joe@mail.com', 'joesmith@email.xyz'],

/*--------------------------------*/

