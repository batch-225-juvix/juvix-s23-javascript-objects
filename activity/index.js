// [SECTION] Real World Application Objects
/*
    - Scenario
    1. We would like to create a game that would have pokemon interact with each other.
    2. Every pokemon would have the same set of stats, properties and functions.

*/


/*
// Creating an object constructor instead will help with this process
function Pokemon(name, level) {

    // Properties
    this.name = name;
    this.level = level;
    this.health = 2 * level;
    this.attack = level;


// (target) means the parameter
    //Methods
    this.tackle = function(target) {
        console.log(this.name + ' tackled ' + target.name);
        target.health -= this.attack;
        console.log( target.name + "'s health is now reduced to " + target.health);
        };
    this.faint = function(){
        console.log(this.name + 'fainted.');
    }

}

// 3 & 8 are the parameters.
// "Pikachu" & 'Rattata' are the argument.
// Creates new instances of the "Pokemon" object each with their unique properties
let pikachu = new Pokemon("Pikachu", 3);
let rattata = new Pokemon('Rattata', 8);

// Providing the "rattata" object as an argument to the "pikachu" tackle method will create interaction between the two objects
pikachu.tackle(rattata);


*/

/*
// tackle which means the statement of  function(target) {
        console.log(this.name + ' tackled ' + target.name);
        target.health -= this.attack;
        console.log( target.name + "'s health is now reduced to " + target.health);
*/

/*----------------------------------------*/

// ACTIVITY:

let player1 = {
    name: "Ash Ketchum",
    age: 10,
    friends: {
        hoenn: ["May", "Max"],
        kanto:["Brock", "Misty"]
    },
    pokemon: 

        ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
    
    
    talk: function () {
       console.log("Hello! I am " + this.name);
     }

}




console.log(player1);
player1.talk();
console.log('Result of dot notation:');

/*-------------------------------------*/

console.log('Result of square bracket notation:')
let player2 =  ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"];

console.log(player2);
console.log('Result of talk method');
console.log('Pikachu! I choose you!');


/*--------------------------------------*/

// console.log("Pokemon Game");



function Pokemon(name, level){

        // Status of the Pokemons:
        this.name = name;
        this.level = level;
        this.health = 2 * level;
        this.attack = level;



        // SKILL / Moveset of the Pokemons:
    this.tackle = function(target) {
        console.log(this.name + ' tackled ' + target.name);
        target.health -= this.attack;
        console.log( target.name + "'s health is now reduced to " + target.health);
        };
    this.faint = function(){
        console.log(this.name + 'fainted.');
    }

    this.thunderbolt = function(target) {
        console.log(this.name + ' thunderbolt ' + target.name);
        target.health -= this.attack;
        console.log( target.name + "'s health is now reduced to " + target.health);
        };

    this.flamethrower = function(target) {
        console.log(this.name + ' flamethrower ' + target.name);
        target.health -= this.attack;
        console.log( target.name + "'s health is now reduced to " + target.health);
        };
    this.waterpump = function(target) {
        console.log(this.name + ' waterpump ' + target.name);
        target.health -= this.attack;
        console.log( target.name + "'s health is now reduced to " + target.health);
        };

}


let pikachu = new Pokemon("Pikachu", 20);
let charizard = new Pokemon("Charizard", 35);
let squirtle = new Pokemon("Squirtle", 16);


// Individual Stats of the pokemon:
console.log(charizard);
console.log(pikachu);
console.log(squirtle);


// Attack mode:
pikachu.tackle(charizard);
pikachu.thunderbolt(charizard);
charizard.flamethrower(squirtle);
squirtle.waterpump(pikachu);
